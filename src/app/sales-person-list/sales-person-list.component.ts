import { Component, OnInit } from '@angular/core';
import {SalesPerson} from './sales-person';

@Component({
  selector: 'app-sales-person-list',
  templateUrl: './sales-person-list.component.html',
  styleUrls: ['./sales-person-list.component.css']
})
export class SalesPersonListComponent implements OnInit {
  salesPersonList: SalesPerson[] = [
    new SalesPerson('Kumar', 'Turuk', 'hljk@g.com', 5000),
    new SalesPerson('Makar', 'Tur', 'com', 5000),
    new SalesPerson('Kumar', 'Turuk', 'hl@g.com', 5000),
    new SalesPerson('Kumar', 'Turuk', 'k@g.com', 5000)
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
